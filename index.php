<?php
	require_once('util.php');
	session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>home</title>
	</head>
	<body>
		<a href="login.php">Log in</a> | <a href="logout.php">Log out</a>

		<h1>Update Weather</h1>

		<form action="updateWeather.php" method="get">
			<input name="temp" type="number" placeholder="Degrees Kelvin">
			<select name="desc">
				<option value="Cloudy">Cloudy</option>
				<option value="Rainy">Rainy</option>
				<option value="Snoww~~~~~~~~~~~~~">Snoww~~~~~~~~~~~~~</option>
			</select>
			<p color="red">[NOTE] You must sign in to update the weather. [NOTE]</p>
			<button>Update</button>
		</form>

		<h1>Current Weather</h1>
		<hr />
		<?php
			require_once('mysqlconnect.php');
			$result = $conn->query("SELECT * FROM weather_data;");
			if ($row = $result->fetch_assoc()) {
				echo "<p>Current temperature: " . $row["temp"] . "&deg; K</p>";
				echo "<p>Description: " . $row["descrip"] . "</p>";
			}
	    ?>
	</body>
</html>
