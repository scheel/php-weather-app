<?php
	session_start();
?>
<?php
    $ldap = ldap_connect("ldap.jmay.us");
    $user = $_POST['username'];

    $authenticated = ldap_bind($ldap, "CN=" . $user . ",CN=phpwall,DC=team1,DC=isucdc,DC=com", $_POST['password']);

    if ($authenticated) {
		$_SESSION['logged_in'] = true;
		$_SESSION['username'] = $user;
        header("Location: index.php");
    } else {
        echo("Login failed.  Please <a href='login.php'>try again</a>.");
    }
?>
