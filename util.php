<?php
function destroy_session() {
    if (session_id() == "") {
        session_start();
    }
    if ( isset( $_COOKIE[session_name()] ) ) {
        setcookie( session_name(), "", time()-3600, "/" );
    }
    $_SESSION = array();
    session_destroy();
}

function verify_session() {
    if (session_id() == "") {
        session_start();
    }
}

function check_authenticated() {
    verify_session();
    if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] == false) {
        return false;
    }
    return true;
}

function require_authenticated() {
    verify_session();
    if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] == false) {
        die("Not logged in.");
    }
}
?>
