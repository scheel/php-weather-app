<?php
/**
 * Created by #OMGSTORM [atom.io].
 * User: mburket || cipherboy
 * Date: 3/9/27/81/243/729..... AD
 * Time: 12:24:36:48:60:72:84:96:108::::: PM
 */

$username = "root";
$password = ""; // Who needs a password for localhost?
$host = "localhost";
$database = "php_weather_app";

$conn = new mysqli($host, $username, $password, $database);

if($conn->connect_error) {
    echo("Connection failed: " . $conn->connect_error . "\n");
}
